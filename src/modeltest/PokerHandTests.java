package modeltest;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.Card;
import model.CardSuit;
import model.CardType;
import model.Hand;
import model.PokerHand;

/**
 * Test the functionality of the PokerHand class
 * 
 * Date : 2/24/16
 * 
 * @author Chad Baily
 * @author Erica Kok
 *
 */

public class PokerHandTests
{

	private Hand myHand;
	private Card myCard;
	private PokerHand myPokerHand;

	@Before
	public void setup()
	{
		myHand = new Hand(5);
	}

	/**
	 * Testing to see if multiple copies of one card are put into a hand, if
	 * they will a PokerHand condition
	 */
	@Test
	public void testIfWrongFourOfKind()
	{
		myCard = new Card(CardSuit.CLUBS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.FIVE, null);
		myHand.add(myCard);

		boolean test = myPokerHand.isFourOfKind();
		assertFalse("Same four cards, should not work", test);
	}

	/**
	 * Testing to see if .isThreeOfKing works for .isFourOfKing, which it should
	 * not because there are four of the same card
	 */
	
	@Test
	public void testIfCountedRight()
	{

		myCard = new Card(CardSuit.CLUBS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.FIVE, null);
		myHand.add(myCard);

		boolean test = myPokerHand.isThreeOfKind();
		assertFalse("Should not work, should be 4 of a kind", test);

	}
	
	/**
	 * Test if it's a Royal Flush.
	 */
	
	@Test
	public void testIfRoyalFlush(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.ACE, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.QUEEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isRoyalFlush();
		
		assertTrue("It should be a Royal Flush!", test);
	}
	
	/**
	 * Test if it's a Straight Flush.
	 */
	
	@Test
	public void testIfStraightFlush(){
		myCard = new Card(CardSuit.HEARTS, CardType.SEVEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.THREE, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.SIX, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.FIVE, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isStraightFlush();
		
		assertTrue("It should be a Straight Flush!", test);
	}
	
	/**
	 * Test if it's a Four of a Kind.
	 */
	
	@Test
	public void testIfFourKind(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();

		boolean test = myPokerHand.isFourOfKind();
		
		assertTrue("It should be a Four of a Kind!", test);
	}
	
	/**
	 * Test if it's a Full House.
	 */
	
	@Test
	public void testIfFullHouse(){
		myCard = new Card(CardSuit.SPADES, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.QUEEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.QUEEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.QUEEN, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isFullHouse();
		
		assertTrue("It should be a Full House!", test);
	}
	
	/**
	 * Test if it's a Flush.
	 */
	
	@Test
	public void testIfFlush(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.TWO, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.SEVEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isFlush();
		
		assertTrue("It should be a Flush!", test);
	}
	
	/**
	 * Test if it's a Straight.
	 */
	
	@Test
	public void testIfStraight(){
		myCard = new Card(CardSuit.HEARTS, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.FIVE, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.SEVEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.SIX, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.EIGHT, null);
		myHand.add(myCard);
		
		myHand.orderCards();

		boolean test = myPokerHand.isStraight();
		
		assertTrue("It should be a Straight!", test);
	}
	
	/**
	 * Test if it's a Three of a Kind.
	 */
	
	@Test
	public void testIfThreeKind(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isThreeOfKind();
		
		assertTrue("It should be a Three of a Kind!", test);
	}
	
	/**
	 * Test if it's a Two Pairs.
	 */
	
	@Test
	public void testIfTwoPairs(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isTwoPair();
		
		assertTrue("It should be a Two Pair!", test);
	}
	
	/**
	 * Test if it's a Pair.
	 */
	
	@Test
	public void testIfOnePair(){
		myCard = new Card(CardSuit.HEARTS, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.TEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.FIVE, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.DIAMONDS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isPair();
		
		assertTrue("It should be a Pair!", test);
	}
	
	/**
	 * Test if it's a HighCard.
	 */
	
	@Test
	public void testIfHighCard(){
		myCard = new Card(CardSuit.DIAMONDS, CardType.SEVEN, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.SPADES, CardType.FOUR, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.CLUBS, CardType.EIGHT, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.JACK, null);
		myHand.add(myCard);

		myCard = new Card(CardSuit.HEARTS, CardType.KING, null);
		myHand.add(myCard);
		
		myHand.orderCards();
		
		boolean test = myPokerHand.isHighCard();
		
		assertTrue("It should be a High Card!", test);
	}
	
}

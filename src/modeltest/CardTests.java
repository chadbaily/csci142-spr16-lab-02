package modeltest;

import model.Card;
import model.CardSuit;
import model.CardType;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Card;
/**
 * Test the functionality of the Card class
 * 
 * Date : 2/24/16
 * @author Chad Baily 
 * @author Erica Kok
 *
 */
public class CardTests
{
	private Card myCard;

	@Before
	public void setup()
	{
		myCard = new Card(CardSuit.HEARTS, CardType.QUEEN, null);

	}

	/**
	 * Test to see if the card has been correctly flipped
	 */
	@Test
	public void testIfFlipped()
	{

		boolean test = myCard.isFaceUp();
		myCard.flip();
		assertTrue("The card was not flipped", test == myCard.isFaceUp());
	}
	
	

}

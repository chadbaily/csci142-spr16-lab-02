package modeltest;

import static org.junit.Assert.*;

import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import model.ComputerPlayer;
import model.Deck;
import model.Hand;
import model.Player;
import model.PokerModel;
/**
 * Test the functionality of the PokerModel class
 * 
 * Date : 2/24/16
 * 
 * @author Chad Baily
 * @author Erica Kok
 *
 */
public class PokerModelTests
{
	PokerModel myPokerModel;
	Player myPlayer;
	ComputerPlayer myComputerPlayer;
	Hand myHand;
	Deck myDeck;

	@Before
	public void setup()
	{
		myPlayer = new Player("ChadErica");
		myPokerModel = new PokerModel(myPlayer);
		myComputerPlayer = new ComputerPlayer("Jack");
		myHand = new Hand(5);
		myDeck = new Deck();
	}

	/**
	 * Test to see if you can draw a card when it is not your turn
	 */
	@Test
	public void testIfComputerPlayerDiscardFirst()
	{
		boolean test = false;
		/*
		 * If the vector of cards to discard is equal to zero
		 */
		if (myComputerPlayer.getHand().discard(myComputerPlayer.selectCardsToDiscard()).size() == 0)
		{
			test = true;
		}

		assertTrue("Cannot discard because its not your turn", test);

	}

	/**
	 * Testing to see if the game has been successfully reset by calling
	 * .resetGame()
	 */
	@Test
	public void testIfReset()
	{
		boolean test = myPokerModel.resetGame();
		assertTrue("The game has been reset", test);
	}
	
	/**
	 * Testing to see if you added the proper amount of card in the Hand.
	 */
	@Test
	public void dealCardsProperAmount()
	{
		Player player = new Player("ChadErica");
		PokerModel myPokerModel = new PokerModel(player);
		myPokerModel.resetGame();
		myPokerModel.dealCards();
		player = myPokerModel.getPlayer(0);
		Hand myHand = player.getHand();
		int numCards = myHand.getNumberCardsInHand();
		
		assertTrue("Wrong number of Cards.", numCards==5);
	}
}

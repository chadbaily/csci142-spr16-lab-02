package model;

/**
 * @purpose 
 * 
 * PokerModel class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

public class PokerModel
{
	private Player[] myPlayer;
	private int myIndexPlayerup;
	private int myMaxRounds;
	private int myRound;

	public PokerModel(Player player)
	{

	}

	public int switchTurns()
	{
		return 0;
	}

	public void dealCards()
	{

	}

	public Player determineWinner()
	{
		return null;
	}

	public boolean resetGame()
	{
		return false;
	}

	public Player getPlayerUp()
	{
		return null;
	}

	public Player getPlayer(int index)
	{
		return null;
	}

	public int getNumberDraws()
	{
		return 0;
	}

	public int getIndexPlayerUp()
	{
		return 0;
	}

}

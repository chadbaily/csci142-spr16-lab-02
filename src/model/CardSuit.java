package model;

/**
 * @purpose 
 * 
 * CardSuit enumeration ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

public enum CardSuit
{
	SPADES ("Spades"),
	DIAMONDS ("Diamonds"),
	HEARTS ("Hearts"),
	CLUBS ("Clubs");
	
	private final String mySuit;
	
	private CardSuit(String name) 
	{
		mySuit = name;
	}
	
	public String getSuit()
	{
		return mySuit;
	}
	

}

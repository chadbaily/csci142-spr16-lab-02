package model;

/**
 * @purpose 
 * 
 * Player class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

public class Player
{
	private static final String DEFAULT_NAME = "Steve";
	private String myName;
	private int myNumberWins;
	private boolean myAmAI;
	Hand myHand;

	public Player(String name)
	{
		myName = name;
		myHand = new Hand(5);
	}

	public boolean validateName(String name)
	{
		if (name.matches("[a-zA-Z]"))
		{
			return true;
		}
		else
			return false;
	}

	public int incrementNumber()
	{
		return 0;
	}

	public String toString()
	{
		return null;
	}

	public Object clone()
	{
		Object myClone = new Object();
		myClone = myName + myNumberWins;
		return myClone;
	}

	public Hand getHand()
	{
		return myHand;
	}

	public String getName()
	{
		return myName;
	}

	public int getNumberWins()
	{
		return myNumberWins;
	}

	public boolean getAmAI()
	{
		if (myAmAI)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

package model;

/**
 * @purpose 
 * 
 * CardType enumeration ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

public enum CardType
{
	ACE (1),
	TWO (2),
	THREE (3),
	FOUR (4),
	FIVE (5),
	SIX (6),
	SEVEN (7),
	EIGHT (8),
	NINE (9),
	TEN (10),
	JACK (11),
	QUEEN (12),
	KING (13);
	
	private final int myType;
	
	private CardType (int type)
	{
		myType = type;
	}
	
	public int getType()
	{
		return myType;
	}

}


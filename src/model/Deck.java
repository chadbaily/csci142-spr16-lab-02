package model;

/**
 * @purpose 
 * 
 * Deck class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

public class Deck
{
	private Vector<Card> myCards;

	public Deck()
	{
		myCards = new Vector<Card>();
		for (CardSuit p : CardSuit.values())
		{
			for (CardType t : CardType.values())
			{
				Card myCard = new Card(p, t, null);
				myCards.add(myCard);
			}
		}
	}

	public boolean constructDeck()
	{
		if (myCards.size() == 52)
		{
			return true;
		}
		else
			return false;
	}

	public Card draw()
	{
		Random rand = new Random();

		int x = rand.nextInt(50) + 1;

		return myCards.get(x);
	}

	public boolean shuffle()
	{
		Object myTest = myCards;
		Collections.shuffle(myCards);
		if (myTest.equals(myCards))
		{
			return false;
		}
		else
			return true;
	}

	public String toString()
	{
		return "Your deck is : " + myCards;
	}

	public Object clone()
	{
		Object myClone = new Object();
		myClone = myCards;
		return myClone;
	}
}

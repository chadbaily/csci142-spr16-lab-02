package model;

/**
 * @purpose
 * 
 * 			PokerHand class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

public class PokerHand extends Hand
{

	private int myNumberCards;
	private int myMaxNumberCards;
	private Hand myHand;

	public PokerHand(int maxNum)
	{
		super(maxNum);

	}

	public int determineRanking()
	{
		return 0;
	}

	public int compareTo(PokerHand pokerHand)
	{
		return 0;
	}

	public String toString()
	{
		return "Your hand is : " + myHand;
	}

	public int getRanking()
	{
		return 0;
	}

	public int getNumberCards()
	{
		return 0;
	}

	public int getMaxNumberCards()
	{
		return 0;
	}

	public boolean isHighCard()
	{
		return false;
	}

	public boolean isPair()
	{
		return false;
	}

	public boolean isTwoPair()
	{
		return false;
	}

	public boolean isThreeOfKind()
	{
		return false;
	}

	public boolean isStraight()
	{
		return false;
	}

	public boolean isFlush()
	{
		return false;
	}

	public boolean isFullHouse()
	{
		return false;
	}

	public boolean isFourOfKind()
	{
		return false;
	}

	public boolean isStraightFlush()
	{
		boolean ascending = false;
		boolean suit = false;
		for (int i = 0; i < myHand.getCards().size(); i++)
		{
			for (int j = 1; i < 5; j++)
			{
				if (myHand.getCards().get(i).getType() + 1 == myHand.getCards().get(j).getType())
				{
					ascending = true;
				}
				else
				{
					ascending = false;
				}
			}
		}

		for (int i = 0; i < myHand.getCards().size(); i++)
		{
			if (myHand.getCards().get(i).getSuit() == myHand.getCards().get(i + 1).getSuit())
			{
				suit = true;
			}
			else
			{
				suit = false;
			}
		}

		if (ascending == true && suit == true)
		{
			return true;
		}
		return false;
	}

	public boolean isRoyalFlush()
	{
		boolean match = true;

		for (int i = 0; i < myHand.getCards().size() - 1; i++)
		{
			match &= myHand.getCards().get(i).getSuit().equals(myHand.getCards().get(i + 1).getSuit());
		}

		if (match && myHand.getCards().get(0).getType() == 1)
		{
			for (int i = 1; i < myHand.getCards().size() - 1; i++)
			{
				for (int j = 10; j < 14; j++)
				{
					if (myHand.getCards().get(i).getType() == j)
					{
						return true;
					}

					break;
				}
			}
		}

		return false;
	}

}

package model;

/**
 * @purpose 
 * 
 * Card class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

import java.awt.*;
import java.util.Vector;


public class Card
{

	private Vector myCard;
	private Image myImage;
	private boolean myIsFaceUp = false;

	public Card(CardSuit suit, CardType type, Image image)
	{
		myCard = new Vector(3);
		myCard.add(suit.getSuit());
		myCard.add(type.getType());
		myCard.add(image);

	}

	public boolean isFaceUp()
	{
		return myIsFaceUp;
	}

	public void flip()
	{
		if (myIsFaceUp == true)
		{
			myIsFaceUp = false;
		}

		if (myIsFaceUp == false)
		{
			myIsFaceUp = true;
		}
	}

	public int getType()
	{
		return (int) myCard.get(1);
	}

	public String getSuit()
	{
		return (String) myCard.get(0);
	}

	public Image getImage()
	{
		return null;
	}

	public String toString()
	{
		if ((int) myCard.get(1) == 1)
		{
			return "Ace of " + (String) myCard.get(0);
		}

		if ((int) myCard.get(1) == 11)
		{
			return "Jack of " + (String) myCard.get(0);
		}

		if ((int) myCard.get(1) == 12)
		{
			return "Queen of " + (String) myCard.get(0);
		}

		if ((int) myCard.get(1) == 13)
		{
			return "King of " + (String) myCard.get(0);
		}
		else
			return (int) myCard.get(1) + " of " + (String) myCard.get(0);
	}

	public Object clone()
	{
		Object myClone = new Object();
		myClone = myCard;
		return myClone;
	}

}

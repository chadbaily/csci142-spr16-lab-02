package model;

/**
 * @purpose 
 * 
 * Hand class ...
 * 
 * @author Chad Baily
 * @author Erica Kok
 * 
 * @dueDate 3//16
 * 
 */

import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class Hand
{

	private Vector<Card> myCards;

	public Hand(int maxNum)
	{
		myCards = new Vector<Card>(maxNum);
	}

	public Vector<Card> getCards()
	{
		return myCards;
	}

	public int getNumberCardsInHand()
	{
		return myCards.size();
	}

	public boolean add(Card card)
	{
		if (myCards.size() < 5)
		{
			if (myTestMultCards(card))
			{
				return false;
			}
			else
			{
				myCards.add(card);
				return true;
			}
		}
		else if (myCards.size() > 5)
		{
			return false;
		}
		return false;
	}

	public Vector<Card> discard(Vector<Integer> indices)
	{
		Vector<Card> myTemp = new Vector<Card>(indices.size());

		for (int i = 0; i < indices.size(); i++)
		{
			myTemp.add(myCards.get(indices.get(i)));
			myCards.remove(indices.get(i));
		}
		return myTemp;
	}

	public String toString()
	{
		return "Your hand is : " + myCards;
	}

	public void orderCards()
	{
		// Collections.sort(myCards);
	}

	public boolean holdCards()
	{
		return false;
		// Works by checking there was a card discarded
	}

	private boolean myTestMultCards(Card card)
	{
		boolean match = false;
		for (int i = 0; i < myCards.size(); i++)
		{
			if (myCards.get(i).getSuit() == card.getSuit() && myCards.get(i).getType() == card.getType())
				match = true;
		}
		return match;
	}
}
